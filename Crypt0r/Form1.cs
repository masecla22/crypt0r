﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Crypt0r
{
    public partial class cryptor : Form
    {
        private FormWindowState m_WindowState = FormWindowState.Normal;
        private Rectangle m_FormSizeAndLocation = Rectangle.Empty;
        private void ChangeWindowState(FormWindowState p_NewState)
        {
            this.WindowState = FormWindowState.Normal;
            switch (p_NewState){
                case FormWindowState.Maximized:
                    if (m_WindowState == FormWindowState.Normal)
                    {
                        m_FormSizeAndLocation.Location = this.Location;
                        m_FormSizeAndLocation.Size = this.Size;
                    }
                    this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    this.Size = SystemInformation.WorkingArea.Size;
                    this.Location = SystemInformation.WorkingArea.Location;
                    break;
                case FormWindowState.Minimized:
                    this.WindowState = FormWindowState.Minimized;
                    break;
                case FormWindowState.Normal:
                    this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
                    this.Location = m_FormSizeAndLocation.Location;
                    this.Size = m_FormSizeAndLocation.Size;
                    break;
            }
            m_WindowState = p_NewState;
        }
        private bool m_MousePressed = false;
        private int m_oldX, m_oldY;
        void topbar_MouseDown(object sender, MouseEventArgs e)
        {
            Point TS = this.PointToScreen(e.Location);
            m_oldX = TS.X;
            m_oldY = TS.Y;
            m_MousePressed = true;
        }
        void topbar_MouseUp(object sender, MouseEventArgs e)
        {
            m_MousePressed = false;
        }
        void topbar_MouseMove(object sender, MouseEventArgs e)
        {
            if (m_MousePressed == true && m_WindowState != FormWindowState.Maximized)
            {
                Point TS = this.PointToScreen(e.Location);

                this.Location = new Point(this.Location.X + (TS.X - m_oldX),
                                          this.Location.Y + (TS.Y - m_oldY));
                m_oldX = TS.X;
                m_oldY = TS.Y;
            }
        }
        public cryptor()
        {
            InitializeComponent();
            topbar.MouseDown += new MouseEventHandler(topbar_MouseDown);
            topbar.MouseMove += new MouseEventHandler(topbar_MouseMove);
            topbar.MouseUp += new MouseEventHandler(topbar_MouseUp);
            title.MouseDown += new MouseEventHandler(topbar_MouseDown);
            title.MouseMove += new MouseEventHandler(topbar_MouseMove);
            title.MouseUp += new MouseEventHandler(topbar_MouseUp);
            decrypt1.Hide();
        }

        private void decrypt_Click(object sender, EventArgs e)
        {
            decrypt1.Show();
            crypt1.Hide();
            moveActive(decrypt.Top);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void crypt_Click(object sender, EventArgs e)
        {
            decrypt1.Hide();
            crypt1.Show();
            moveActive(crypt.Top);
        }

        private void topbar_Paint(object sender, PaintEventArgs e)
        {

        }

        public void moveActive(int toY)
        {
                this.Refresh();
                if (activeOptionPanel.Location.Y < toY)
                {
                    for (int i = activeOptionPanel.Location.Y; i < toY; i += (toY - activeOptionPanel.Location.Y + 16) / 16)
                    {
                        this.Refresh();
                        activeOptionPanel.Location = new Point(activeOptionPanel.Location.X, i + 1);
                        Thread.Sleep(10);
                    }
                }
                else
                {
                    for (int i = activeOptionPanel.Location.Y; i > toY; i -= (activeOptionPanel.Location.Y - toY + 16) / 16)
                    {
                        this.Refresh();
                        activeOptionPanel.Location = new Point(activeOptionPanel.Location.X, i - 1);
                        Thread.Sleep(10);
                    }
                }
        }

    }
}
