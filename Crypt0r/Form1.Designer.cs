﻿namespace Crypt0r
{
    partial class cryptor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cryptor));
            this.panel1 = new System.Windows.Forms.Panel();
            this.activeOptionPanel = new System.Windows.Forms.Panel();
            this.decrypt = new System.Windows.Forms.Button();
            this.crypt = new System.Windows.Forms.Button();
            this.topbar = new System.Windows.Forms.Panel();
            this.title = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.crypt1 = new Crypt0r.crypt();
            this.decrypt1 = new Crypt0r.decrypt();
            this.panel1.SuspendLayout();
            this.topbar.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.panel1.Controls.Add(this.activeOptionPanel);
            this.panel1.Controls.Add(this.decrypt);
            this.panel1.Controls.Add(this.crypt);
            this.panel1.Location = new System.Drawing.Point(0, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(130, 607);
            this.panel1.TabIndex = 0;
            // 
            // activeOptionPanel
            // 
            this.activeOptionPanel.BackColor = System.Drawing.Color.Red;
            this.activeOptionPanel.Location = new System.Drawing.Point(119, 1);
            this.activeOptionPanel.Name = "activeOptionPanel";
            this.activeOptionPanel.Size = new System.Drawing.Size(11, 34);
            this.activeOptionPanel.TabIndex = 2;
            // 
            // decrypt
            // 
            this.decrypt.FlatAppearance.BorderSize = 0;
            this.decrypt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.decrypt.Font = new System.Drawing.Font("Microsoft YaHei UI Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.decrypt.ForeColor = System.Drawing.SystemColors.ControlText;
            this.decrypt.Image = ((System.Drawing.Image)(resources.GetObject("decrypt.Image")));
            this.decrypt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.decrypt.Location = new System.Drawing.Point(0, 35);
            this.decrypt.Name = "decrypt";
            this.decrypt.Size = new System.Drawing.Size(130, 35);
            this.decrypt.TabIndex = 1;
            this.decrypt.Text = "Decrypt";
            this.decrypt.UseVisualStyleBackColor = true;
            this.decrypt.Click += new System.EventHandler(this.decrypt_Click);
            // 
            // crypt
            // 
            this.crypt.FlatAppearance.BorderSize = 0;
            this.crypt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.crypt.Font = new System.Drawing.Font("Microsoft YaHei UI Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crypt.ForeColor = System.Drawing.SystemColors.ControlText;
            this.crypt.Image = ((System.Drawing.Image)(resources.GetObject("crypt.Image")));
            this.crypt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.crypt.Location = new System.Drawing.Point(0, 0);
            this.crypt.Name = "crypt";
            this.crypt.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.crypt.Size = new System.Drawing.Size(130, 35);
            this.crypt.TabIndex = 0;
            this.crypt.Text = "Crypt";
            this.crypt.UseVisualStyleBackColor = true;
            this.crypt.Click += new System.EventHandler(this.crypt_Click);
            // 
            // topbar
            // 
            this.topbar.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.topbar.Controls.Add(this.title);
            this.topbar.Controls.Add(this.button1);
            this.topbar.Location = new System.Drawing.Point(0, 0);
            this.topbar.Name = "topbar";
            this.topbar.Size = new System.Drawing.Size(673, 26);
            this.topbar.TabIndex = 1;
            this.topbar.Paint += new System.Windows.Forms.PaintEventHandler(this.topbar_Paint);
            // 
            // title
            // 
            this.title.AutoSize = true;
            this.title.Font = new System.Drawing.Font("Microsoft YaHei UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title.Location = new System.Drawing.Point(285, 2);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(65, 21);
            this.title.TabIndex = 2;
            this.title.Text = "Crypt0r";
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft YaHei UI Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(644, -1);
            this.button1.Margin = new System.Windows.Forms.Padding(0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(29, 28);
            this.button1.TabIndex = 1;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // crypt1
            // 
            this.crypt1.Location = new System.Drawing.Point(131, 26);
            this.crypt1.Name = "crypt1";
            this.crypt1.Size = new System.Drawing.Size(542, 593);
            this.crypt1.TabIndex = 2;
            // 
            // decrypt1
            // 
            this.decrypt1.Location = new System.Drawing.Point(130, 26);
            this.decrypt1.Name = "decrypt1";
            this.decrypt1.Size = new System.Drawing.Size(542, 607);
            this.decrypt1.TabIndex = 3;
            // 
            // cryptor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(672, 633);
            this.Controls.Add(this.crypt1);
            this.Controls.Add(this.topbar);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.decrypt1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "cryptor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Crypt0r";
            this.panel1.ResumeLayout(false);
            this.topbar.ResumeLayout(false);
            this.topbar.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button crypt;
        private System.Windows.Forms.Panel topbar;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label title;
        private System.Windows.Forms.Button decrypt;
        private System.Windows.Forms.Panel activeOptionPanel;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private crypt crypt1;
        private decrypt decrypt1;
    }
}

