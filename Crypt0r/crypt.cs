﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Crypt0r
{
    public partial class crypt : UserControl
    {
        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }
        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
        public crypt()
        {
            InitializeComponent();
            comboBox1.SelectedIndex = 0;
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            String key = textBox1.Text;
            String keyAux = key;
            int am = 0;
            Int32.TryParse(textBox2.Text, out am);
            for (int i = 0; i < am-1; i++)
                key += keyAux;
            if (am == 0)
            {
                richTextBox2.Text = richTextBox1.Text;
                return;
            }
            String text = richTextBox1.Text;
            Thread f = new Thread(delegate () {
                text = applyEncrypt(text, key);
            });
            f.Start();
            f.Join();
            if (comboBox1.SelectedIndex == 0)
                richTextBox2.Text = text;
            else if (comboBox1.SelectedIndex == 1)
            {
                richTextBox2.Text = "";
                byte[] b = Encoding.GetEncoding(437).GetBytes(text);
                for (int i = 0; i < b.Length; i++)
                    richTextBox2.Text += b[i].ToString() + " ";
            }
            else if (comboBox1.SelectedIndex == 2)
            {
                byte[] b = Encoding.GetEncoding(437).GetBytes(text);
                string a = ByteArrayToString(b);
                richTextBox2.Text = a;
            }
        }
        public static String encrypt1(String a)
        {
            char[] arrs = a.ToCharArray();
            for (int i = 0; i < a.Length / 2; i++)
            {
                char temp = arrs[i];
                arrs[i] = arrs[a.Length - i - 1];
                arrs[a.Length - i - 1] = temp;
            }
            return new string(arrs);
        }
        public static String decrypt1(String a)
        {
            return encrypt1(a);
        }
        public static String encrypt2(String a)
        {
            byte[] bytes = Encoding.GetEncoding(437).GetBytes(a);
            for (int i = 0; i < a.Length; i++)
                bytes[i] = atb(bytes[i], 3);
            return Encoding.GetEncoding(437).GetString(bytes);
        }
        public static String decrypt2(String a)
        {
            byte[] bytes = Encoding.GetEncoding(437).GetBytes(a);
            for (int i = 0; i < a.Length; i++)
                bytes[i] = atb(bytes[i], -3);
            return Encoding.GetEncoding(437).GetString(bytes);
        }

        public static String encrypt3(String a)
        {
            byte[] bytes = Encoding.GetEncoding(437).GetBytes(a);
            for (int i = 0; i < a.Length; i++)
                bytes[i] = atb(bytes[i], a.Length);
            return Encoding.GetEncoding(437).GetString(bytes);
        }
        public static String decrypt3(String a)
        {
            byte[] bytes = Encoding.GetEncoding(437).GetBytes(a);
            for (int i = 0; i < a.Length; i++)
                bytes[i] = atb(bytes[i], -a.Length);
            return Encoding.GetEncoding(437).GetString(bytes);
        }
        public static byte atb(byte b, int amount)
        {
            return (byte)(((int)b - amount) % 256);
        }
        public static String encrypt4(string a)
        {
            byte[] bytes = Encoding.GetEncoding(437).GetBytes(a);
            for (uint i = 0; i < a.Length - 2; i += 2)
            {
                byte chr = bytes[i];
                bytes[i] = bytes[i + 1];
                bytes[i + 1] = chr;
            }
            return Encoding.GetEncoding(437).GetString(bytes);
        }
        public static String decrypt4(string a)
        {
            return encrypt4(a);
        }

        public static String encrypt5(string a, string key)
        {
            byte[] bytes = Encoding.GetEncoding(437).GetBytes(a), keybytes = Encoding.GetEncoding(437).GetBytes(key);
            for (uint i = 0; i < a.Length - 2; i += 2)
                bytes[i] = atb(bytes[i], (int)keybytes[i % key.Length]);
            return Encoding.GetEncoding(437).GetString(bytes);
        }
        public static String decrypt5(string a, string key)
        {
            byte[] bytes = Encoding.GetEncoding(437).GetBytes(a), keybytes = Encoding.GetEncoding(437).GetBytes(key);
            for (uint i = 0; i < a.Length - 2; i += 2)
                bytes[i] = atb(bytes[i], -(int)keybytes[i % key.Length]);
            return Encoding.GetEncoding(437).GetString(bytes);
        }
        public static String encrypt6(string a)
        {
            for (int i = 1; i < a.Length; i += 2)
                a = a.Insert(i, getRandomChar());
            return a;
        }
        public static String decrypt6(string a)
        {
            string result = "";
            for (int i = 0; i < a.Length; i += 2)
                result += a.ElementAt(i);
            return result;
        }

        public static String applyEncrypt(String a, String key)
        {
            for (int i = 0; i < key.Length; i++)
            {
                char c = key.ElementAt(i);
                switch (c)
                {
                    case '1':
                        a = encrypt1(a);
                        break;
                    case '2':
                        a = encrypt2(a);
                        break;
                    case '3':
                        a = encrypt3(a);
                        break;
                    case '4':
                        a = encrypt4(a);
                        break;
                    case '5':
                        a = encrypt5(a, key);
                        break;
                    case '6':
                        a = encrypt6(a);
                        break;
                }
            }
            return a;
        }
        public static String applyDecrypt(String a, String key)
        {
            for (int i = key.Length - 1; i >= 0; i--)
            {
                char c = key.ElementAt(i);
                switch (c)
                {
                    case '1':
                        a = decrypt1(a);
                        break;
                    case '2':
                        a = decrypt2(a);
                        break;
                    case '3':
                        a = decrypt3(a);
                        break;
                    case '4':
                        a = decrypt4(a);
                        break;
                    case '5':
                        a = decrypt5(a, key);
                        break;
                    case '6':
                        a = decrypt6(a);
                        break;
                }
            }
            return a;
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            richTextBox1_TextChanged(sender, e);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
                textBox1.Text = "1";
            richTextBox1_TextChanged(sender, e);
        }

        private void random_Click(object sender, EventArgs e)
        {
            textBox1.Text = randomKey(14) + "6" + randomKey(20);
        }
        public String randomKey(int length)
        {
            var chars = "12345";
            var stringChars = new char[length + 1];
            var random = new Random();
            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }
            String finalString = new String(stringChars);
            return finalString;
        }
        public static Random randozm = new Random();
        public static String getRandomChar()
        {
            String charset = "ABCDEFGHIJKLMNOPQRSTUVWZYXabcdefghijklmnopqrstuvwzyx0123456789";
            return new string(new char[] { charset.ToCharArray()[randozm.Next(0, charset.Length - 1)] });
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            richTextBox1_TextChanged(sender, e);
        }
    }
}
