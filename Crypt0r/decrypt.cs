﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Crypt0r
{
    public partial class decrypt : UserControl
    {
        public decrypt()
        {
            InitializeComponent();
            comboBox1.SelectedIndex = 0;
        }

        private void random_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        public void richTextBox1_TextChanged_1(object sender, EventArgs e)
        {
            String todec = "";
            if (comboBox1.SelectedIndex == 0)
            {
                todec = richTextBox1.Text;
            }
            else if (comboBox1.SelectedIndex == 1)
            {
                string text = richTextBox1.Text.Replace('\n', ' ');
                string[] strs = text.Split(' ');
                int cont = 0;
                for (int i = 0; i < strs.Length; i++)
                {
                    byte b;
                    bool succ = Byte.TryParse(strs[i], out b);
                    if (succ)
                        cont++;
                }
                byte[] bs = new byte[cont];
                cont = 0;
                for (int i = 0; i < strs.Length; i++)
                {
                    byte b;
                    bool succ = Byte.TryParse(strs[i], out b);
                    if (succ)
                    {
                        bs[cont] = b;
                        cont++;
                    }
                }
                todec = Encoding.GetEncoding(437).GetString(bs);
            }
            else if (comboBox1.SelectedIndex == 2)
            {
                try
                {
                    todec = Encoding.GetEncoding(437).GetString(crypt.StringToByteArray(richTextBox1.Text));
                }
                catch (Exception ex)
                {
                    todec = "MALFORMED INPUT";
                    richTextBox2.Text = todec;
                    return;
                }
            }
            String key = "";
            int am = 0;
            Int32.TryParse(textBox2.Text, out am);
            for (int i = 0; i < am; i++)
                key += textBox1.Text;
            richTextBox2.Text = crypt.applyDecrypt(todec,key);
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {
            richTextBox1_TextChanged_1(sender, e);
        }

        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            richTextBox1_TextChanged_1(sender, e);
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            richTextBox1_TextChanged_1(sender, e);
        }
    }
}
